package com.talkweb.library.adapter.base.util;

import ohos.multimodalinput.event.TouchEvent;

public class TouchEventUtil {

    public static String getTouchAction(int actionId) {
        String actionName = "Unknow:id=" + actionId;
        switch (actionId) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                actionName = "ACTION_DOWN";
                break;
            case TouchEvent.POINT_MOVE:
                actionName = "ACTION_MOVE";
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                actionName = "ACTION_UP";
                break;
            case TouchEvent.CANCEL:
                actionName = "ACTION_CANCEL";
                break;
            case TouchEvent.NONE:
                actionName = "ACTION_OUTSIDE";
                break;
            default:
                break;
        }
        return actionName;
    }

}
