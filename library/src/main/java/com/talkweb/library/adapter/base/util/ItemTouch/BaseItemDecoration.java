package com.talkweb.library.adapter.base.util.ItemTouch;

import androidx.annotation.NonNull;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;

public abstract class BaseItemDecoration {
    /**
     * Draw any appropriate decorations into the Canvas supplied to the RecyclerView.
     * Any content drawn by this method will be drawn before the item views are drawn,
     * and will thus appear underneath the views.
     *
     * @param c Canvas to draw into
     * @param parent RecyclerView this ItemDecoration is drawing into
     */
    public void onDraw(@NonNull Canvas c, @NonNull ListContainer parent) {
        onDraw(c, parent);
    }

    /**
     * Draw any appropriate decorations into the Canvas supplied to the RecyclerView.
     * Any content drawn by this method will be drawn after the item views are drawn
     * and will thus appear over the views.
     *
     * @param c Canvas to draw into
     * @param parent RecyclerView this ItemDecoration is drawing into
     */
    public void onDrawOver(@NonNull Canvas c, @NonNull ListContainer parent) {
        onDrawOver(c, parent);
    }

    /**
     * @deprecated

     */
    @Deprecated
    public void getItemOffsets(@NonNull Rect outRect, int itemPosition,
                               @NonNull ListContainer parent) {
        outRect.set(0, 0, 0, 0);
    }

}
