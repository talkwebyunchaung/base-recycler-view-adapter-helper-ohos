package com.talkweb.library.adapter.base.listener;

import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.multimodalinput.event.TouchEvent;


import static com.talkweb.library.adapter.base.BaseQuickAdapter.EMPTY_VIEW;
import static com.talkweb.library.adapter.base.BaseQuickAdapter.FOOTER_VIEW;
import static com.talkweb.library.adapter.base.BaseQuickAdapter.HEADER_VIEW;
import static com.talkweb.library.adapter.base.BaseQuickAdapter.LOADING_VIEW;

/**
 * Created by AllenCoder on 2016/8/03.
 * <p>
 * This can be useful for applications that wish to implement various forms of click and longclick and childComponent click
 * manipulation of item views within the ListContainer. SimpleClickListener may intercept
 * a touch interaction already in progress even if the SimpleClickListener is already handling that
 * gesture stream itself for the purposes of scrolling.
 *
 */
public abstract class BaseSimpleClickListener implements ListContainer.ItemClickedListener, Component.TouchEventListener {
    public static String TAG = "SimpleClickListener";

    private ListContainer recyclerComponent;
    protected BaseQuickAdapter baseQuickAdapter;
    private boolean mIsPrepressed = false;
    private boolean mIsShowPress = false;
    private Component mPressedComponent = null;

    public boolean onInterceptTouchEvent(ListContainer rv, TouchEvent e) {
        if (recyclerComponent == null) {
            this.recyclerComponent = rv;
            this.baseQuickAdapter = (BaseQuickAdapter) recyclerComponent.getItemProvider();
        } else if (recyclerComponent != rv) {
            this.recyclerComponent = rv;
            this.baseQuickAdapter = (BaseQuickAdapter) recyclerComponent.getItemProvider();
        }

        return false;
    }


    @Override
    public boolean onTouchEvent(Component rv, TouchEvent e) {
        return false ;
    }

    /**
     * Callback method to be invoked when an item in this AdapterComponent has
     * been clicked.
     * @param adapter
     * @param view     The view within the AdapterComponent that was clicked (this
     *                 will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     */
    public abstract void onItemClick(BaseQuickAdapter adapter, Component view, int position);

    /**
     * callback method to be invoked when an item in this view has been
     * click and held
     * @param adapter
     * @param view     The view whihin the AbsListComponent that was clicked
     * @param position The position of the view int the adapter
     * @return true if the callback consumed the long click ,false otherwise
     */
    public abstract void onItemLongClick(BaseQuickAdapter adapter, Component view, int position);
    /**
     * callback method to be invoked when an itemchild in this view has been click
     * @param adapter
     * @param view     The view whihin the AbsListComponent that was clicked
     * @param position The position of the view int the adapter
     * @return true if the callback consumed the long click ,false otherwise
     */
    public abstract void onItemChildClick(BaseQuickAdapter adapter, Component view, int position);
    /**
     * callback method to be invoked when an item in this view has been
     * click and held
     * @param adapter
     * @param view     The view whihin the AbsListComponent that was clicked
     * @param position The position of the view int the adapter
     * @return true if the callback consumed the long click ,false otherwise
     */
    public abstract void onItemChildLongClick(BaseQuickAdapter adapter, Component view, int position);

    public boolean inRangeOfComponent(Component view, TouchEvent ev) {
        int[] location;
        if (view == null || !view.isComponentDisplayed()) {
            return false;
        }
        location = view.getLocationOnScreen();
        int x = location[0];
        int y = location[1];
        if (ev.getPointerPosition(0).getX() < x
                || ev.getPointerPosition(0).getX() > (x + view.getWidth())
                || ev.getPointerPosition(0).getY() < y
                || ev.getPointerPosition(0).getY() > (y + view.getHeight())) {
            return false;
        }
        return true;
    }

    private boolean isHeaderOrFooterPosition(int position) {
        /**
         *  have a headview and EMPTY_VIEW FOOTER_VIEW LOADING_VIEW
         */
        if (baseQuickAdapter == null) {
            if (recyclerComponent != null) {
                baseQuickAdapter = (BaseQuickAdapter) recyclerComponent.getItemProvider();
            } else {
                return false;
            }
        }
        int type = baseQuickAdapter.getItemComponentType(position);
        return (type == EMPTY_VIEW || type == HEADER_VIEW || type == FOOTER_VIEW || type == LOADING_VIEW);
    }

    private boolean isHeaderOrFooterComponent(int type) {
        return (type == EMPTY_VIEW || type == HEADER_VIEW || type == FOOTER_VIEW || type == LOADING_VIEW);
    }
}


