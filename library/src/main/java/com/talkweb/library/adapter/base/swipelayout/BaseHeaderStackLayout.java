package com.talkweb.library.adapter.base.swipelayout;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * @author: jun.xiong
 * @date: 2021/5/17 14:59
 * @description:
 */
public abstract class BaseHeaderStackLayout extends StackLayout {

    public enum RefreshState {
        /**
         * 初始态
         */
        STATE_INIT,
        /**
         * 下拉刷新的头部可见
         */
        STATE_VISIBLE,
        /**
         * 正在刷新的状态
         */
        STATE_REFRESH,
        /**
         * 超出可刷新距离的状态
         */
        STATE_OVER,
        /**
         * 超出刷新位置松开手后的状态
         */
        STATE_OVER_RELEASE
    }

    protected RefreshState mState = RefreshState.STATE_INIT;
    /**
     * 触发下拉刷新时的最小高度，当刚好下拉到这个距离，那就直接刷新，
     * 如果下拉的距离超过了这个距离，那就先滚动到这个距离，然后才开始刷新
     */
    public int mPullRefreshHeight;
    /**
     * 最小阻尼，用户越往下拉，越不跟手
     */
    public float minDamp = 1.6f;
    /**
     * 最大阻尼
     */
    public float maxDamp = 2.2f;

    protected BaseHeaderStackLayout(Context context) {
        this(context,null);
    }

    protected BaseHeaderStackLayout(Context context, AttrSet attrSet) {
        this(context, attrSet,"");
    }

    protected BaseHeaderStackLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPullRefreshHeight = AttrHelper.vp2px(80, context);
        init();
    }

    /**
     * 初始化
     */
    protected abstract void init();

    /**
     * 滚动
     *
     * @param scrollY           纵轴滚动的距离
     * @param pullRefreshHeight 触发下拉刷新时的最小高度
     */
    public abstract void onScroll(int scrollY, int pullRefreshHeight);

    /**
     * 显示头部组件
     */
    public abstract void onVisible();

    /**
     * 超出头部视图高度，松手开始加载
     */
    public abstract void onOver();

    /**
     * 开始刷新
     */
    public abstract void onRefresh();

    /**
     * 刷新完成
     */
    public abstract void onFinish();

    public void setState(RefreshState state) {
        this.mState = state;
    }

    public RefreshState getState() {
        return mState;
    }

}
