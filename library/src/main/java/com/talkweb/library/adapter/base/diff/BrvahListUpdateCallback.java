package com.talkweb.library.adapter.base.diff;

import androidx.annotation.Nullable;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.diff.ListUpdateCallback;

public class BrvahListUpdateCallback implements ListUpdateCallback {

    private BaseQuickAdapter mAdapter;

    public BrvahListUpdateCallback(BaseQuickAdapter adapter){
        mAdapter = adapter;
    }

    @Override
    public void onInserted(int position, int count) {
        mAdapter.notifyDataSetItemRangeInserted(position + mAdapter.getHeaderLayoutCount(), count);
    }

    @Override
    public void onRemoved(int position, int count) {
        if (mAdapter.getLoadMoreViewCount() > 0 && mAdapter.getCount() == 0) {
            // 如果注册了加载更多，并且当前itemCount为0，则需要加上loadMore所占用的一行
            mAdapter.notifyDataSetItemRangeRemoved(position + mAdapter.getHeaderLayoutCount(), count + 1);
        } else {
            mAdapter.notifyDataSetItemRangeRemoved(position + mAdapter.getHeaderLayoutCount(), count);
        }
    }

    @Override
    public void onMoved(int fromPosition, int toPosition) {
        mAdapter.onItemMoved(fromPosition + mAdapter.getHeaderLayoutCount(), toPosition + mAdapter.getHeaderLayoutCount());
    }

    @Override
    public void onChanged(int position, int count, @Nullable Object payload) {
        mAdapter.notifyDataSetItemRangeChanged(position + mAdapter.getHeaderLayoutCount(), count);
    }
}