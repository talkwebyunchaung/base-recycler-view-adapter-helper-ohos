package com.talkweb.library.adapter.base;


import androidx.annotation.NonNull;
import com.talkweb.library.adapter.base.listener.OnItemDragListener;
import com.talkweb.library.adapter.base.listener.OnItemSwipeListener;
import com.talkweb.library.adapter.base.util.ItemTouch.BaseItemTouchHelper;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Collections;
import java.util.List;

/**
 * Created by luoxw on 2016/7/13.
 */
public abstract class BaseItemDraggableAdapter<T, K extends BaseViewHolder> extends BaseQuickAdapter<T, K> {

    private static final int NO_TOGGLE_VIEW = 0;
    protected int mToggleViewId = NO_TOGGLE_VIEW;
    protected BaseItemTouchHelper mItemTouchHelper;
    protected boolean itemDragEnabled = false;
    protected boolean itemSwipeEnabled = false;
    protected OnItemDragListener mOnItemDragListener;
    protected OnItemSwipeListener mOnItemSwipeListener;
    protected boolean mDragOnLongPress = true;

    protected Component.TouchEventListener mOnToggleViewTouchListener;
    protected Component.LongClickedListener mOnToggleViewLongClickListener;

    protected BaseItemDraggableAdapter(List<T> data) {
        super(data);
    }

    protected BaseItemDraggableAdapter(int layoutResId, List<T> data) {
        super(layoutResId, data);
    }


    /**
     * To bind different types of holder and solve different the bind events
     *
     * @param holder
     * @param position
     * @see #getDefItemViewType(int)
     */
    @Override
    public void onBindViewHolder(@NonNull K holder, int position) {
        super.onBindViewHolder(holder, position);
        int viewType = holder.getItemViewType();

        if (mItemTouchHelper != null && itemDragEnabled && viewType != LOADING_VIEW && viewType != HEADER_VIEW
                && viewType != EMPTY_VIEW && viewType != FOOTER_VIEW && hasToggleView()) {
            Component toggleView = holder.getView(mToggleViewId);
            if (toggleView != null) {
                toggleView.setTag(holder);
                if (mDragOnLongPress) {
                    toggleView.setLongClickedListener(mOnToggleViewLongClickListener);
                } else {
                    toggleView.setTouchEventListener(mOnToggleViewTouchListener);
                }
            }
        }
    }


    /**
     * Set the toggle view's id which will trigger drag event.
     * If the toggle view id is not set, drag event will be triggered when the item is long pressed.
     *
     * @param toggleViewId the toggle view's id
     */
    public void setToggleViewId(int toggleViewId) {
        mToggleViewId = toggleViewId;
    }

    /**
     * Is there a toggle view which will trigger drag event.
     */
    public boolean hasToggleView() {
        return mToggleViewId != NO_TOGGLE_VIEW;
    }

    /**
     * Set the drag event should be trigger on long press.
     * Work when the toggleViewId has been set.
     *
     * @param longPress by default is true.
     */
    public void setToggleDragOnLongPress(boolean longPress) {
        mDragOnLongPress = longPress;
        if (mDragOnLongPress) {
            mOnToggleViewTouchListener = null;
            generToggleViewLongClickListener(mItemTouchHelper, itemDragEnabled);
        } else {
            generToggleViewTouchListener(mItemTouchHelper, itemDragEnabled);
            mOnToggleViewLongClickListener = null;
        }
    }

    private void generToggleViewLongClickListener(BaseItemTouchHelper touchHelper, boolean itemDragEnabled) {
        mOnToggleViewLongClickListener = new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component v) {
                if (touchHelper != null && itemDragEnabled) {
                    touchHelper.startDrag((BaseViewHolder) v.getTag());
                }
            }
        };
    }

    private void generToggleViewTouchListener(BaseItemTouchHelper touchHelper, boolean itemDragEnabled) {
        mOnToggleViewTouchListener = new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent event) {
                if (event.getAction() == TouchEvent.PRIMARY_POINT_DOWN
                        && !mDragOnLongPress) {
                    if (touchHelper != null && itemDragEnabled) {
                        touchHelper.startDrag((BaseViewHolder) component.getTag());
                    }
                    return true;
                } else {
                    return false;
                }
            }
        };
    }

    /**
     * Enable drag items.
     * Use itemView as the toggleView when long pressed.
     *
     * @param itemTouchHelper {@link BaseItemTouchHelper}
     */
    public void enableDragItem(@NonNull BaseItemTouchHelper itemTouchHelper) {
        enableDragItem(itemTouchHelper, NO_TOGGLE_VIEW, true);
    }

    /**
     * Enable drag items. Use the specified view as toggle.
     *
     * @param itemTouchHelper {@link BaseItemTouchHelper}
     * @param toggleViewId    The toggle view's id.
     */
    public void enableDragItem(@NonNull BaseItemTouchHelper itemTouchHelper, int toggleViewId) {
        enableDragItem(itemTouchHelper, toggleViewId, true);
    }

    /**
     * Enable drag items. Use the specified view as toggle.
     *
     * @param itemTouchHelper {@link BaseItemTouchHelper}
     * @param toggleViewId    The toggle view's id.
     * @param dragOnLongPress If true the drag event will be trigger on long press, otherwise on touch down.
     */
    public void enableDragItem(@NonNull BaseItemTouchHelper itemTouchHelper, int toggleViewId, boolean dragOnLongPress) {
        itemDragEnabled = true;
        mItemTouchHelper = itemTouchHelper;
        setToggleViewId(toggleViewId);
        setToggleDragOnLongPress(dragOnLongPress);
    }

    /**
     * Disable drag items.
     */
    public void disableDragItem() {
        itemDragEnabled = false;
        mItemTouchHelper = null;
    }

    public boolean isItemDraggable() {
        return itemDragEnabled;
    }

    /**
     * <p>Enable swipe items.</p>
     * You should attach {@link BaseItemTouchHelper} which construct with {@link } to the Recycler when you enable this.
     */
    public void enableSwipeItem() {
        itemSwipeEnabled = true;
    }

    public void disableSwipeItem() {
        itemSwipeEnabled = false;
    }

    public boolean isItemSwipeEnable() {
        return itemSwipeEnabled;
    }

    /**
     * @param onItemDragListener Register a callback to be invoked when drag event happen.
     */
    public void setOnItemDragListener(OnItemDragListener onItemDragListener) {
        mOnItemDragListener = onItemDragListener;
    }

    public int getViewHolderPosition(BaseViewHolder viewHolder) {
        return viewHolder.getPosition() - getHeaderLayoutCount();
    }

    public void onItemDragStart(BaseViewHolder viewHolder) {
        if (mOnItemDragListener != null && itemDragEnabled) {
            mOnItemDragListener.onItemDragStart(viewHolder, getViewHolderPosition(viewHolder));
        }
    }

    public void onItemDragMoving(BaseViewHolder source, BaseViewHolder target) {
        int from = getViewHolderPosition(source);
        int to = getViewHolderPosition(target);

        if (inRange(from) && inRange(to)) {
            if (from < to) {
                for (int i = from; i < to; i++) {
                    Collections.swap(mData, i, i + 1);
                }
            } else {
                for (int i = from; i > to; i--) {
                    Collections.swap(mData, i, i - 1);
                }
            }
            onItemMoved(source.getPosition(), target.getPosition());
        }

        if (mOnItemDragListener != null && itemDragEnabled) {
            mOnItemDragListener.onItemDragMoving(source, from, target, to);
        }
    }

    public void onItemDragEnd(BaseViewHolder viewHolder) {
        if (mOnItemDragListener != null && itemDragEnabled) {
            mOnItemDragListener.onItemDragEnd(viewHolder, getViewHolderPosition(viewHolder));
        }
    }

    public void setOnItemSwipeListener(OnItemSwipeListener listener) {
        mOnItemSwipeListener = listener;
    }

    public void onItemSwipeStart(BaseViewHolder viewHolder) {
        if (mOnItemSwipeListener != null && itemSwipeEnabled) {
            mOnItemSwipeListener.onItemSwipeStart(viewHolder, getViewHolderPosition(viewHolder));
        }
    }

    public void onItemSwipeClear(BaseViewHolder viewHolder) {
        if (mOnItemSwipeListener != null && itemSwipeEnabled) {
            mOnItemSwipeListener.clearView(viewHolder, getViewHolderPosition(viewHolder));
        }
    }

    public void onItemSwiped(BaseViewHolder viewHolder) {
        final int pos = getViewHolderPosition(viewHolder);
        if (inRange(pos)) {
            mData.remove(pos);
            notifyDataSetItemRemoved(viewHolder.getPosition());

            if (mOnItemSwipeListener != null && itemSwipeEnabled) {
                mOnItemSwipeListener.onItemSwiped(viewHolder, pos);
            }
        }
    }

    public void onItemSwiping(Canvas canvas, BaseViewHolder viewHolder, float dX, float dY, boolean isCurrentlyActive) {
        if (mOnItemSwipeListener != null && itemSwipeEnabled) {
            mOnItemSwipeListener.onItemSwipeMoving(canvas, viewHolder, dX, dY, isCurrentlyActive);
        }
    }

    private boolean inRange(int position) {
        return position >= 0 && position < mData.size();
    }
}
