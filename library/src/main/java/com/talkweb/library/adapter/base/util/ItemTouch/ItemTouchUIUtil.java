package com.talkweb.library.adapter.base.util.ItemTouch;

import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;

public interface ItemTouchUIUtil {

    void onDraw(Canvas c, ListContainer recyclerView, Component view,
                float dX, float dY, int actionState, boolean isCurrentlyActive);

    void onDrawOver(Canvas c, ListContainer recyclerView, Component view,
                    float dX, float dY, int actionState, boolean isCurrentlyActive);

    void clearView(Component view);

    void onSelected(Component view);

}
