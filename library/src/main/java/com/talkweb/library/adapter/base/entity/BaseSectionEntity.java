package com.talkweb.library.adapter.base.entity;

import java.io.Serializable;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public abstract class BaseSectionEntity<T> implements Serializable {
    public boolean isHeader;
    public T t;
    public String header;

    public BaseSectionEntity(boolean isHeader, String header) {
        this.isHeader = isHeader;
        this.header = header;
        this.t = null;
    }

    public BaseSectionEntity(T t) {
        this.isHeader = false;
        this.header = null;
        this.t = t;
    }

    @Override
    public String toString() {
        return "SectionEntity{" +
                "isHeader=" + isHeader +
                ", t=" + t +
                ", header='" + header + '\'' +
                '}';
    }
}
