package com.talkweb.library.adapter.base.listener;


import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;

/**
 * Created by AllenCoder on 2016/8/03.
 * <p>
 * <p>
 * A convenience class to extend when you only want to OnItemClickListener for a subset
 * of all the SimpleClickListener. This implements all methods in the
 * {@link BaseSimpleClickListener}
 */
public abstract class BaseOnItemClickListener extends BaseSimpleClickListener {
    @Override
    public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {
        onSimpleItemClick(adapter, view, position);
    }

    @Override
    public void onItemLongClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemChildLongClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    public abstract void onSimpleItemClick(BaseQuickAdapter adapter, Component view, int position);
}
