package com.talkweb.library.adapter.base.animation;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class SlideInRightAnimation implements BaseAnimation {
    @Override
    public Animator[] getAnimators(Component view) {

        AnimatorProperty property = view.createAnimatorProperty();
        AnimatorProperty transX = property.moveFromX( 100).moveToX(0);

        return new Animator[]{
                //ObjectAnimator.ofFloat(view, "translationX", view.getRootView().getWidth(), 0)
                transX
        };
    }
}
