package com.talkweb.library.adapter.base.animation;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;


/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class SlideInBottomAnimation implements BaseAnimation {
    @Override
    public Animator[] getAnimators(Component view) {

        AnimatorProperty property = view.createAnimatorProperty();
        AnimatorProperty transY = property.moveFromY(view.getHeight()).moveToY(0);

        return new Animator[]{
                //ObjectAnimator.ofFloat(view, "translationY", view.getMeasuredHeight(), 0)
                transY
        };
    }
}
