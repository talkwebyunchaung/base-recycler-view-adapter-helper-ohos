package com.talkweb.library.adapter.base.listener;


import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;

/**
 * Created by AllenCoder on 2016/8/03.
 * A convenience class to extend when you only want to OnItemChildLongClickListener for a subset
 * of all the SimpleClickListener. This implements all methods in the
 * {@link BaseSimpleClickListener}
 **/
public abstract class BaseOnItemChildLongClickListener extends BaseSimpleClickListener {
    @Override
    public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemLongClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemChildLongClick(BaseQuickAdapter adapter, Component view, int position) {
        onSimpleItemChildLongClick(adapter, view, position);
    }

    public abstract void onSimpleItemChildLongClick(BaseQuickAdapter adapter, Component view, int position);
}
