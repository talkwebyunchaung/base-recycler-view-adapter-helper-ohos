package com.talkweb.library.adapter.base.swipelayout;

import com.talkweb.library.ResourceTable;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * @author: jun.xiong
 * @date: 2021/2/7 11:25
 * @description:
 */
public class CommonRefreshHeader extends BaseHeaderStackLayout {

    private Image mImage;
    private Text mText;
    private AnimatorProperty mAnimator;

    public CommonRefreshHeader(Context context) {
        super(context);
    }

    public CommonRefreshHeader(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public CommonRefreshHeader(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_refresh_view, this, true);
        mImage = (Image) findComponentById(ResourceTable.Id_image);
        mText = (Text) findComponentById(ResourceTable.Id_text);

    }

    @Override
    public void onScroll(int scrollY, int pullRefreshHeight) {

    }

    @Override
    public void onVisible() {
        mText.setText("下拉刷新");
    }

    @Override
    public void onOver() {
        mText.setText("松开刷新");
    }

    @Override
    public void onRefresh() {
        mText.setText("正在刷新...");
        progressAnimation();
    }

    private void progressAnimation() {
        mAnimator = mImage.createAnimatorProperty();
        mAnimator.setDuration(700).rotate(360).setLoopedCount(AnimatorProperty.INFINITE).setTarget(mImage).start();
    }

    @Override
    public void onFinish() {
        mAnimator.cancel();
    }
}
