package com.talkweb.library.adapter.base.animation;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;


/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class ScaleInAnimation implements BaseAnimation {
    private static final float DEFAULT_SCALE_FROM = .5f;
    private final float mFrom;

    public ScaleInAnimation() {
        this(DEFAULT_SCALE_FROM);
    }

    public ScaleInAnimation(float from) {
        mFrom = from;
    }

    @Override
    public Animator[] getAnimators(Component view) {
        AnimatorProperty property = view.createAnimatorProperty();
        AnimatorProperty  scaleX =property.scaleXFrom(mFrom).scaleX(1f);
        AnimatorProperty  scaleY =property.scaleYFrom(mFrom).scaleY(1f);
        return new Animator[]{scaleX, scaleY};
    }
}
