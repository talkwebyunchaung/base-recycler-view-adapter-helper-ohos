package com.talkweb.floatingbutton.util;

import ohos.app.Context;

final public class Util {

    private Util() {
    }

    public static int vpToPx(Context context, float vp) {
        final float scale = context.getResourceManager().getDeviceCapability().screenDensity / 160;
        return Math.round(vp * scale);
    }
}
