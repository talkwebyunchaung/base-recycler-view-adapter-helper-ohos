package com.talkweb.brvah;

import com.talkweb.brvah.slice.SectionMultipleItemUseAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SectionMultipleItemUseAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SectionMultipleItemUseAbilitySlice.class.getName());
    }
}
