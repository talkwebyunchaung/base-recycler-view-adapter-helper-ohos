package com.talkweb.brvah;

import com.talkweb.brvah.slice.ItemClickSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * @author: jun.xiong
 * @date: 2021/5/13 09:48
 * @description:
 */
public class ItemClickUseAbility extends Ability {


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setMainRoute(ItemClickSlice.class.getName());
    }
}
