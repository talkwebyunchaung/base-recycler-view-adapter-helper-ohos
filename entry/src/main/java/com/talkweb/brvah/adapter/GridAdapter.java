package com.talkweb.brvah.adapter;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.HomeItem;
import com.talkweb.brvah.util.AppUtils;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * The GridAdapter
 */
public class GridAdapter {
    private static final int GRID_LAYOUT_BORDER_MARGIN = 20;
    private static final int GRID_ITEM_RIGHT_MARGIN = 8;

    private final List<Component> componentList = new ArrayList<>();

    public GridAdapter(Context context, List<HomeItem> itemInfos) {
        int itemPx = getItemWidthByScreen(context);
        for (HomeItem item : itemInfos) {
            Component gridItem = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_home_item_view, null, false);
            gridItem.setTag(item.getTitle());
            Image imageItem = (Image) gridItem.findComponentById(ResourceTable.Id_grid_item_image);
            imageItem.setPixelMap(item.getImageResource());
            Text textItem = (Text) gridItem.findComponentById(ResourceTable.Id_grid_item_text);
            textItem.setText(item.getTitle());
            gridItem.setWidth(itemPx);
            gridItem.setHeight(itemPx);
            gridItem.setMarginRight(AttrHelper.vp2px(GRID_ITEM_RIGHT_MARGIN, context));
            gridItem.setMarginBottom(AttrHelper.vp2px(GRID_ITEM_RIGHT_MARGIN, context));
            componentList.add(gridItem);
        }
    }

    /**
     * method for get componentList
     *
     * @return componentList
     */
    public List<Component> getComponentList() {
        return componentList;
    }

    private int getItemWidthByScreen(Context context) {
        int screenWidth = AppUtils.getScreenInfo(context).getPointXToInt();

        return (screenWidth
                - AttrHelper.vp2px(GRID_LAYOUT_BORDER_MARGIN, context)
                - AttrHelper.vp2px(GRID_ITEM_RIGHT_MARGIN, context) * 3)
                / 3;
    }
}
