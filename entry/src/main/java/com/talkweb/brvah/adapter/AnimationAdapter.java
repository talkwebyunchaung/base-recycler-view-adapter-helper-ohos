package com.talkweb.brvah.adapter;


import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.Status;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * 文 件 名: AnimationAdapter
 * 创 建 人: Allen
 * 创建日期: 16/12/24 15:33
 * 邮   箱: AllenCoder@126.com
 * 修改时间：
 * 修改备注：
 */
public class AnimationAdapter extends BaseQuickAdapter<Status, BaseViewHolder> {
    public AnimationAdapter() {
        super(ResourceTable.Layout_item_animation, getSampleData(100));
    }

    @Override
    protected void convert( BaseViewHolder helper, Status item) {
        ((Text) helper.getView(ResourceTable.Id_text)).setText(helper.getPosition()+"");
    }

    public static List<Status> getSampleData(int lenth) {
        List<Status> list = new ArrayList<>();
        for (int i = 0; i < lenth; i++) {
            Status status = new Status();
            status.setUserName("Chad" + i);
            status.setCreatedAt("04/05/" + i);
            status.setRetweet(i % 2 == 0);
            status.setUserAvatar("https://avatars1.githubusercontent.com/u/7698209?v=3&s=460");
            status.setText("BaseRecyclerViewAdpaterHelper https://www.recyclerview.org");
            list.add(status);
        }
        return list;
    }

}
