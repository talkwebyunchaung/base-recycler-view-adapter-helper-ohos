package com.talkweb.brvah.adapter;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.library.adapter.base.BaseItemDraggableAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author: jun.xiong
 * @date: 2021/5/12 15:38
 * @description:
 */
public class ItemDragAdapter extends BaseItemDraggableAdapter<String, BaseViewHolder> {
    public ItemDragAdapter(List<String> data) {
        super(ResourceTable.Layout_item_drag_view, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        switch (helper.getPosition() % 3) {
            case 0:
                helper.setImageResource(ResourceTable.Id_iv_head, ResourceTable.Media_head_img0);
                break;
            case 1:
                helper.setImageResource(ResourceTable.Id_iv_head, ResourceTable.Media_head_img1);
                break;
            case 2:
                helper.setImageResource(ResourceTable.Id_iv_head, ResourceTable.Media_head_img2);
                break;
            default:
                break;
        }
        helper.setText(ResourceTable.Id_tv_title, item);
    }
}
