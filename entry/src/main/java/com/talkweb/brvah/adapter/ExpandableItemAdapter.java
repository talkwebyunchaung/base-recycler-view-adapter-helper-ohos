package com.talkweb.brvah.adapter;


import androidx.annotation.NonNull;
import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.Level0Item;
import com.talkweb.brvah.entity.Level1Item;
import com.talkweb.brvah.entity.Person;
import com.talkweb.library.adapter.base.BaseMultiItemQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import com.talkweb.library.adapter.base.entity.IExpandable;
import com.talkweb.library.adapter.base.entity.MultiItemEntity;
import ohos.agp.components.Component;

import java.util.List;

/**
 * Created by luoxw on 2016/8/9.
 */
public class ExpandableItemAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    private static final String TAG = ExpandableItemAdapter.class.getSimpleName();

    public static final int TYPE_LEVEL_0 = 0;
    public static final int TYPE_LEVEL_1 = 1;
    public static final int TYPE_PERSON = 2;

    private boolean isOnlyExpandOne = false;

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public ExpandableItemAdapter(List<MultiItemEntity> data) {
        super(data);
        addItemType(TYPE_LEVEL_0, ResourceTable.Layout_item_expandable_lv0);
        addItemType(TYPE_LEVEL_1, ResourceTable.Layout_item_expandable_lv1);
        addItemType(TYPE_PERSON, ResourceTable.Layout_item_expandable_lv2);
    }


    @Override
    protected void convert(@NonNull final BaseViewHolder holder, final MultiItemEntity item) {
        switch (holder.getItemViewType()) {
            case TYPE_LEVEL_0:
                switch (holder.getPosition() % 3) {
                    case 0:
                        holder.setImageResource(ResourceTable.Id_iv_head, ResourceTable.Media_head_img0);
                        break;
                    case 1:
                        holder.setImageResource(ResourceTable.Id_iv_head, ResourceTable.Media_head_img1);
                        break;
                    case 2:
                        holder.setImageResource(ResourceTable.Id_iv_head, ResourceTable.Media_head_img2);
                        break;
                    default:
                        break;
                }
                final Level0Item lv0 = (Level0Item) item;
                holder.setText(ResourceTable.Id_title, lv0.title)
                        .setText(ResourceTable.Id_sub_title, lv0.subTitle)
                        .setImageResource(ResourceTable.Id_iv, lv0.isExpanded() ? ResourceTable.Media_arrow_b : ResourceTable.Media_arrow_r);
                holder.getItemView().setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component v) {
                        int pos = holder.getPosition();
                        if (lv0.isExpanded()) {
                            collapse(pos);
                        } else if (isOnlyExpandOne) {
                            IExpandable willExpandItem = (IExpandable) getData().get(pos);
                            for (int i = getHeaderLayoutCount(); i < getData().size(); i++) {
                                IExpandable expandable = (IExpandable) getData().get(i);
                                if (expandable.isExpanded()) {
                                    collapse(i);
                                }
                            }
                            expand(getData().indexOf(willExpandItem) + getHeaderLayoutCount());
                        } else {
                            expand(pos);
                        }
                    }
                });
                break;
            case TYPE_LEVEL_1:
                final Level1Item lv1 = (Level1Item) item;
                holder.setText(ResourceTable.Id_title, lv1.title)
                        .setText(ResourceTable.Id_sub_title, lv1.subTitle)
                        .setImageResource(ResourceTable.Id_iv, lv1.isExpanded() ? ResourceTable.Media_arrow_b : ResourceTable.Media_arrow_r);
                holder.getItemView().setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component v) {
                        int pos = holder.getPosition();
                        if (lv1.isExpanded()) {
                            collapse(pos, false);
                        } else {
                            expand(pos, false);
                        }
                    }
                });

                holder.getItemView().setLongClickedListener(new Component.LongClickedListener() {
                    @Override
                    public void onLongClicked(Component v) {
                        int pos = holder.getPosition();
                        // 先获取到当前 item 的父 positon，再移除自己
                        int positionAtAll = getParentPositionInAll(pos);
                        remove(pos);
                        if (positionAtAll != -1) {
                            IExpandable multiItemEntity = (IExpandable) getData().get(positionAtAll);
                            if (!hasSubItems(multiItemEntity)) {
                                remove(positionAtAll);
                            }
                        }
                    }
                });
                break;
            case TYPE_PERSON:
                final Person person = (Person) item;
                holder.setText(ResourceTable.Id_tv, person.name + " parent pos: " + getParentPosition(person));
                holder.getItemView().setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component view) {
                        int pos = holder.getPosition();
                        // 先获取到当前 item 的父 positon，再移除自己
                        int positionAtAll = getParentPositionInAll(pos);
                        remove(pos);
                        if (positionAtAll != -1) {
                            IExpandable multiItemEntity = (IExpandable) getData().get(positionAtAll);
                            if (!hasSubItems(multiItemEntity)) {
                                remove(positionAtAll);
                            }
                        }
                    }
                });
                break;
            default:
                break;
        }
    }

    public boolean isOnlyExpandOne() {
        return isOnlyExpandOne;
    }

    public void setOnlyExpandOne(boolean onlyExpandOne) {
        isOnlyExpandOne = onlyExpandOne;
    }
}
