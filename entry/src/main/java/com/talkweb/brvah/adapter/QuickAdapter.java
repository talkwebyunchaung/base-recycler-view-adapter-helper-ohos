package com.talkweb.brvah.adapter;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.Status;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;

public class QuickAdapter extends BaseQuickAdapter<Status, BaseViewHolder> {

    public QuickAdapter(int dataSize) {
        super(ResourceTable.Layout_item_image_text_view, DataServer.getSampleData(dataSize));
    }

    @Override
    protected void convert(BaseViewHolder helper, Status item) {
        helper.setText(ResourceTable.Id_tv, item.getUserName());
        switch (helper.getPosition() % 3) {
            case 0:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img1);
                break;
            case 1:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img2);
                break;
            case 2:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img3);
                break;
            default:
                break;
        }
    }
}
