package com.talkweb.brvah;

import com.talkweb.brvah.slice.SectionUseSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * @author: jun.xiong
 * @date: 2021/5/10 11:02
 * @description:
 */
public class SectionUseAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setMainRoute(SectionUseSlice.class.getName());
    }
}
