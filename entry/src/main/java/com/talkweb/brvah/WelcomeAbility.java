package com.talkweb.brvah;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * @author: jun.xiong
 * @date: 2021/5/8 09:30
 * @description:
 */
public class WelcomeAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_welcome);

        getUITaskDispatcher().delayDispatch(() -> {
            Intent in = new Intent();
            in.setElementName("", getBundleName(), MainAbility.class.getSimpleName());
            startAbility(in);
            terminateAbility();
        }, 1000);
    }
}
