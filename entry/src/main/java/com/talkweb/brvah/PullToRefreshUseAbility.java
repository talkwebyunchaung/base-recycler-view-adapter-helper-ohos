package com.talkweb.brvah;

import com.talkweb.brvah.slice.PullToRefreshSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * @author: jun.xiong
 * @date: 2021/5/18 15:24
 * @description:
 */
public class PullToRefreshUseAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setMainRoute(PullToRefreshSlice.class.getName());
    }
}
