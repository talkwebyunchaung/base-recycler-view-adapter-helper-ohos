package com.talkweb.brvah;

import com.talkweb.brvah.slice.EmptyViewUseSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * @author: jun.xiong
 * @date: 2021/5/11 12:00
 * @description:
 */
public class EmptyViewUseAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    setMainRoute(EmptyViewUseSlice.class.getName());
    }
}
