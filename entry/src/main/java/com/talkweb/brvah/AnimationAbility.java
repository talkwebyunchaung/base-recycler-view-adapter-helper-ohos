package com.talkweb.brvah;

import com.talkweb.brvah.slice.AnimationSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * @program: brvah
 * @description:
 * @author: LZ
 * @create: 2021-05-10 11:28
 **/
public class AnimationAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(AnimationSlice.class.getName());
    }

    public static void start(Ability ability){

        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder().withBundleName("com.talkweb.brvah")
                .withAbilityName(AnimationAbility.class.getName())
                .build();

        intent.setOperation(operation);
        ability.startAbility(intent);

    }
}