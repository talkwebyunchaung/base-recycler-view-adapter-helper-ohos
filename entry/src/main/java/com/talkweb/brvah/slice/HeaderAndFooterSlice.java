package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.AnimationAdapter;
import com.talkweb.brvah.adapter.HeaderAndFooterAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

/**
 * @program: brvah
 * @description:
 * @author: LZ
 * @create: 2021-05-10 11:30
 **/
public class HeaderAndFooterSlice extends BaseSlice {


    private HeaderAndFooterAdapter headerAndFooterAdapter;
    private static final int PAGE_SIZE = 3;

    private ListContainer mRecyclerView;
    private LayoutScatter mLayoutScatter;
    @Override
    public int getLayout() {
        return ResourceTable.Layout_layout_animation_list;
    }

    private void initAdapter() {
        headerAndFooterAdapter = new HeaderAndFooterAdapter(PAGE_SIZE);
        headerAndFooterAdapter.openLoadAnimation();
        mRecyclerView.setItemProvider(headerAndFooterAdapter);
//        mRecyclerView.addOnItemTouchListener(new OnItemClickListener() {
//            @Override
//            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
//                Toast.makeText(HeaderAndFooterUseActivity.this, "" + Integer.toString(position), Toast.LENGTH_LONG).show();
//            }
//        });
//        headerAndFooterAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                adapter.setNewData(DataServer.getSampleData(PAGE_SIZE));
//                Toast.makeText(HeaderAndFooterUseActivity.this, "" + Integer.toString(position), Toast.LENGTH_LONG).show();
//            }
//        });

    }

    @Override
    protected void initWidget() {

        mLayoutScatter =LayoutScatter.getInstance(this);

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_list);

        initAdapter();


        Component headerView = getHeaderView(0, new Component.ClickedListener() {

            @Override
            public void onClick(Component component) {
                headerAndFooterAdapter.addHeaderView(getHeaderView(1, getRemoveHeaderListener()), 0);

            }
        });
        headerAndFooterAdapter.addHeaderView(headerView);


        Component footerView = getFooterView(0, new Component.ClickedListener() {

            @Override
            public void onClick(Component component) {
                headerAndFooterAdapter.addFooterView(getFooterView(1, getRemoveFooterListener()), 0);

            }
        });
        headerAndFooterAdapter.addFooterView(footerView, 0);
    }

    private Component.ClickedListener getRemoveFooterListener() {
        return new Component.ClickedListener() {

            @Override
            public void onClick(Component component) {
                headerAndFooterAdapter.removeFooterView(component);
            }
        };
    }

    private Component.ClickedListener getRemoveHeaderListener() {
        return new Component.ClickedListener() {

            @Override
            public void onClick(Component component) {
                headerAndFooterAdapter.removeHeaderView(component);
            }
        };
    }


    private Component getHeaderView(int type, Component.ClickedListener listener) {

        Component view =mLayoutScatter.parse(ResourceTable.Layout_head_view,null,false);

        if (type == 1) {
            Image imageView = (Image) view.findComponentById(ResourceTable.Id_iv);
            imageView.setPixelMap(ResourceTable.Media_rm_icon);
        }
        view.setClickedListener(listener);
        return view;
    }

    private Component getFooterView(int type, Component.ClickedListener listener) {

        Component view = mLayoutScatter.parse(ResourceTable.Layout_foot_view, null, false);


        if (type == 1) {
            Image imageView = (Image) view.findComponentById(ResourceTable.Id_iv);
            imageView.setPixelMap(ResourceTable.Media_rm_icon);
        }
        view.setClickedListener(listener);
        return view;
    }

    @Override
    protected void initData() {



    }


}