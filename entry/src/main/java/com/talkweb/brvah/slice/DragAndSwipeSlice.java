package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.ItemDragAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.util.DialogUtil;
import com.talkweb.brvah.util.LogUtils;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import com.talkweb.library.adapter.base.listener.OnItemDragListener;
import com.talkweb.library.adapter.base.util.ItemTouch.BaseItemTouchHelper;
import ohos.agp.components.*;
import ohos.agp.utils.MimeData;

import java.util.List;

/**
 * @author: jun.xiong
 * @date: 2021/5/12 15:26
 * @description:
 */
public class DragAndSwipeSlice extends BaseSlice {

    private ListContainer listContainer;

    @Override
    public int getLayout() {
        return ResourceTable.Layout_layout_animation_list;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("DragAndSwipe Use");
        listContainer = findView(ResourceTable.Id_list);
    }

    @Override
    protected void initData() {
        List<String> mData = DataServer.getStrData();
        ItemDragAdapter mAdapter = new ItemDragAdapter(mData);

//        ItemDragAndSwipeCallback mItemDragAndSwipeCallback = new ItemDragAndSwipeCallback(mAdapter);
        BaseItemTouchHelper mItemTouchHelper = new BaseItemTouchHelper();
        mAdapter.enableDragItem(mItemTouchHelper, ResourceTable.Layout_item_drag_view, true);
        mAdapter.setToggleDragOnLongPress(false);
//        mItemDragAndSwipeCallback.setSwipeMoveFlags(ItemTouchHelper.START | ItemTouchHelper.END);
//        mAdapter.enableSwipeItem();
//        mAdapter.setOnItemSwipeListener(new OnItemSwipeListener() {
//            @Override
//            public void onItemSwipeStart(BaseViewHolder viewHolder, int pos) {
//
//            }
//
//            @Override
//            public void clearView(BaseViewHolder viewHolder, int pos) {
//
//            }
//
//            @Override
//            public void onItemSwiped(BaseViewHolder viewHolder, int pos) {
//
//            }
//
//            @Override
//            public void onItemSwipeMoving(Canvas canvas, BaseViewHolder viewHolder, float dX, float dY, boolean isCurrentlyActive) {
//
//            }
//        });

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {
                DialogUtil.toastShort(getContext(), "点击了" + position);
            }
        });

        mAdapter.setOnItemDragListener(new OnItemDragListener() {
            @Override
            public void onItemDragStart(BaseViewHolder viewHolder, int pos) {
                LogUtils.syso("拖动开始:" + pos);
            }

            @Override
            public void onItemDragMoving(BaseViewHolder source, int from, BaseViewHolder target, int to) {
                LogUtils.syso("拖动中from-" + from + ",to-" + to);

            }

            @Override
            public void onItemDragEnd(BaseViewHolder viewHolder, int pos) {
                LogUtils.syso("拖动结束：" + pos);
            }
        });
        listContainer.setItemProvider(mAdapter);
//        dragItem();
    }

    private Component selectedView;
    private boolean isViewOnDrag;

    private void dragItem() {
        listContainer.setItemLongClickedListener(new ListContainer.ItemLongClickedListener() {
            @Override
            public boolean onItemLongClicked(ListContainer listContainer, Component component, int i, long l) {
                LogUtils.syso(i + "==" + l + "长按：" + component.getTag().toString());

                Component shadowComponent = getShadow();
                shadowComponent.setWidth(component.getWidth());
                shadowComponent.setHeight(component.getHeight());
                shadowComponent.setAlpha(0.8f);
                Component.DragFeedbackProvider dragFeedbackProvider =
                        new Component.DragFeedbackProvider(shadowComponent);
                component.startDragAndDrop(new MimeData(), dragFeedbackProvider);
                component.setVisibility(Component.INVISIBLE);
                selectedView = component;
                isViewOnDrag = true;

                bindComponentTransition();

                return true;
            }
        });
    }

    private void bindComponentTransition() {
        if (listContainer != null && listContainer.getComponentTransition() == null) {
            ComponentTransition transition = new ComponentTransition();
            transition.removeTransitionType(ComponentTransition.SELF_GONE);
            transition.removeTransitionType(ComponentTransition.OTHERS_GONE);
            transition.removeTransitionType(ComponentTransition.CHANGING);
            listContainer.setComponentTransition(transition);
        }
    }

    private Component getShadow() {
        Component itemLayout =
                LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_drag_view, null, false);
        if (itemLayout.findComponentById(ResourceTable.Id_iv_head) instanceof Image) {
            Image imageItem = (Image) itemLayout.findComponentById(ResourceTable.Id_iv_head);
            imageItem.setPixelMap(ResourceTable.Media_head_img0);
            imageItem.setScale(1.2f, 1.2f);
        }
        return itemLayout;
    }
}