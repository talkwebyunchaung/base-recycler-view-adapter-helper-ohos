package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.MultipleItemQuickAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.MultipleItem;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.List;

public class MultipleItemUseAbilitySlice extends BaseSlice {

    private ListContainer mListContainer;
    private MultipleItemQuickAdapter mMultipleItemAdapter;

    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_multiple_item_use;
    }

    @Override
    protected void initWidget() {
        setTitle("MultipleItem Use");
        setBackBtn();

        mListContainer = (ListContainer) findComponentById(ResourceTable.Id_listcontainer);
        final List<MultipleItem> data = DataServer.getMultipleItemData();
        mMultipleItemAdapter = new MultipleItemQuickAdapter(data);
        mListContainer.setItemProvider(mMultipleItemAdapter);
    }

    @Override
    protected void initData() {
        mMultipleItemAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mMultipleItemAdapter.addData(DataServer.getMultipleItemData());
                mMultipleItemAdapter.loadMoreComplete();
            }
        }, mListContainer);
    }

}
