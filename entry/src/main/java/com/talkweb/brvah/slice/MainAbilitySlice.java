package com.talkweb.brvah.slice;

import com.talkweb.brvah.EmptyViewUseAbility;
import com.talkweb.brvah.MainAbility;
import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.SectionUseAbility;
import com.talkweb.brvah.*;
import com.talkweb.brvah.adapter.GridAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.component.GridView;
import com.talkweb.brvah.entity.HomeItem;
import com.talkweb.brvah.util.LogUtils;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import java.util.ArrayList;

public class MainAbilitySlice extends BaseSlice {

    private static final Class<?>[] ACTIVITY = {
            AnimationAbility.class,
            MultipleItemUseAbility.class,
            HeaderAndFooterAbility.class,
            PullToRefreshUseAbility.class,
            SectionUseAbility.class,
            EmptyViewUseAbility.class,
            DragAndSwipeUseAbility.class,
            ItemClickUseAbility.class,
            ExpandableUseAbility.class,
            MainAbility.class,
            UpFetchUseAbility.class,
            SectionMultipleItemUseAbility.class,
            DiffUtilAbility.class
    };
    private static final String[] TITLE = {"Animation", "MultipleItem", "Header/Footer", "PullToRefresh", "Section", "EmptyView", "DragAndSwipe", "ItemClick", "ExpandableItem", "DataBinding", "UpFetchData", "SectionMultipleItem", "DiffUtil"};
    private static final int[] IMG = {ResourceTable.Media_gv_animation, ResourceTable.Media_gv_multipleltem, ResourceTable.Media_gv_header_and_footer, ResourceTable.Media_gv_pulltorefresh, ResourceTable.Media_gv_section, ResourceTable.Media_gv_empty, ResourceTable.Media_gv_drag_and_swipe, ResourceTable.Media_gv_item_click, ResourceTable.Media_gv_expandable, ResourceTable.Media_gv_databinding, ResourceTable.Media_gv_animation, ResourceTable.Media_gv_multipleltem, ResourceTable.Media_gv_databinding};
    private GridView mGridView ;

    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_main;
    }

    @Override
    protected void initWidget() {
        mGridView = findView(ResourceTable.Id_main_list);
    }

    @Override
    protected void initData() {
        ArrayList<HomeItem> mDataList = new ArrayList<>();
        for (int i = 0; i < TITLE.length; i++) {
            HomeItem item = new HomeItem();
            item.setTitle(TITLE[i]);
            item.setActivity(ACTIVITY[i]);
            item.setImageResource(IMG[i]);
            mDataList.add(item);
        }
        GridAdapter adapter = new GridAdapter(getContext(), mDataList);
        mGridView.setAdapter(adapter, new GridView.GridClickListener() {

            @Override
            public void onItemClick(Component component, int position) {
                Intent intent = new Intent();
                intent.setElementName("", getBundleName(), ACTIVITY[position].getName());
                startAbility(intent);

                LogUtils.info("点击了"+ position);
            }
        });
    }

}
