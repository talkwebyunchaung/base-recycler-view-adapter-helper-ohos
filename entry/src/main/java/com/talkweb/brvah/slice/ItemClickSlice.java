package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.ItemClickAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.ClickEntity;
import com.talkweb.brvah.util.DialogUtil;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: jun.xiong
 * @date: 2021/5/13 09:52
 * @description:
 */
public class ItemClickSlice extends BaseSlice {

    private ListContainer listContainer;
    private ItemClickAdapter adapter;

    @Override
    public int getLayout() {
        return ResourceTable.Layout_layout_animation_list;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("ItemClick Use");
        listContainer = findView(ResourceTable.Id_list);
    }

    @Override
    protected void initData() {
        List<ClickEntity> data = new ArrayList<>();
        data.add(new ClickEntity(ClickEntity.CLICK_ITEM_VIEW,"ClickItem"));
        data.add(new ClickEntity(ClickEntity.CLICK_ITEM_CHILD_VIEW,"ClickChild"));
        data.add(new ClickEntity(ClickEntity.LONG_CLICK_ITEM_VIEW,"LongClickItem"));
        data.add(new ClickEntity(ClickEntity.LONG_CLICK_ITEM_CHILD_VIEW,"LongClick"));
        data.add(new ClickEntity(ClickEntity.NEST_CLICK_ITEM_CHILD_VIEW,"NestClick"));
        adapter = new ItemClickAdapter(data);
        adapter.openLoadAnimation();
        listContainer.setItemProvider(adapter);

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {
                DialogUtil.toastShort(getContext(), "onItemClick:" + position);
            }
        });

//        无效，对item设置长按事件，无法被触发，跟list默认长按事件处理有关
//        adapter.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(BaseQuickAdapter adapter, Component view, int position) {
//                DialogUtil.toastShort(getContext(), "onItemLongClick:" + position);
//                return true;
//            }
//        });

//        默认的有效
        /*listContainer.setItemLongClickedListener(new ListContainer.ItemLongClickedListener() {
            @Override
            public boolean onItemLongClicked(ListContainer listContainer, Component component, int i, long l) {
                DialogUtil.toastShort(getContext(), "List-onItemLongClick:" + i);
                return true;
            }
        });*/
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {
                DialogUtil.toastShort(getContext(), "onItemChildClick:" + position);
            }
        });
        adapter.setOnItemChildLongClickListener(new BaseQuickAdapter.OnItemChildLongClickListener() {
            @Override
            public boolean onItemChildLongClick(BaseQuickAdapter adapter, Component view, int position) {
                DialogUtil.toastShort(getContext(), "onItemChildLongClick:" + position);
                return true;
            }
        });

    }
}
