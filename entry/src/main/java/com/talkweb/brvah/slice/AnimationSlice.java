package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.AnimationAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.ListContainer;

/**
 * @program: brvah
 * @description:
 * @author: LZ
 * @create: 2021-05-10 11:30
 **/
public class AnimationSlice extends BaseSlice {


    private ListContainer listContainer;
    private AnimationAdapter animationAdapter;
    @Override
    public int getLayout() {
        return ResourceTable.Layout_layout_animation_list;
    }

    @Override
    protected void initWidget() {
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list);
    }

    @Override
    protected void initData() {
        animationAdapter =new AnimationAdapter();
        animationAdapter.openLoadAnimation();
        int mFirstPageItemCount = 3;
        animationAdapter.setNotDoAnimationCount(mFirstPageItemCount);
        animationAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        listContainer.setItemProvider(animationAdapter);
    }

}