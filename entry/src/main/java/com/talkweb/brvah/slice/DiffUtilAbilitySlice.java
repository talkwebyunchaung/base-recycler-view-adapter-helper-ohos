package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.diffUtil.DiffDemoCallback;
import com.talkweb.brvah.adapter.diffUtil.DiffUtilAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.DiffUtilDemoEntity;
import com.talkweb.library.adapter.base.diff.DiffUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;

public class DiffUtilAbilitySlice extends BaseSlice {

    private ListContainer mRecyclerView;
    private Component itemChangeBtn;
    private Component notifyChangeBtn;
    private Component asyncChangeBtn;
    private DiffUtilAdapter mAdapter;


    //private ExecutorService fixedThreadPool = ThreadPoolExecutor.newFixedThreadPool(2);

    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_diff_util;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("DiffUtil Use");

        findView();
        initRv();
        initClick();
    }

    @Override
    protected void initData() {

    }

    private void findView() {
        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_diff_rv);
        itemChangeBtn = findComponentById(ResourceTable.Id_item_change_btn);
        notifyChangeBtn = findComponentById(ResourceTable.Id_notify_change_btn);
        asyncChangeBtn = findComponentById(ResourceTable.Id_async_change_btn);
    }

    private void initRv() {
        mAdapter = new DiffUtilAdapter(DataServer.getDiffUtilDemoEntities());
        mAdapter.bindToRecyclerView(mRecyclerView);

        Component view = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_head_view, mRecyclerView, false);
        view.findComponentById(ResourceTable.Id_iv).setVisibility(Component.HIDE);
        mAdapter.addHeaderView(view);
    }

    private void initClick() {
        // Use sync example. 同步使用示例
        itemChangeBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                List<DiffUtilDemoEntity> newData = getNewList();
                DiffDemoCallback callback = new DiffDemoCallback(newData);
                mAdapter.setNewDiffData(callback);
            }
        });

        // Use async example. 异步使用示例
        asyncChangeBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                /* 方法一：（快速使用）
                如果只需要简单快速的使用异步刷新，可直接使用此方法，但有可能有内存泄漏的风险；
                建议使用同一个全局的线程池，将其进行传递。
                 */
                List<DiffUtilDemoEntity> newData = getNewList();
                DiffDemoCallback callback = new DiffDemoCallback(newData);
                mAdapter.setNewAsyncDiffData(callback);
            }
        });

        // Just modify a row of data. 仅仅修改某一行数据
        notifyChangeBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                // change item 0
                mAdapter.getData().set(0, new DiffUtilDemoEntity(
                        0,
                        "😊😊Item " + 0,
                        "Item " + 0 + " content have change (notifyItemChanged)",
                        "06-12"));
                mAdapter.refreshNotifyItemChanged(0);
            }
        });
    }

    /**
     * get new data
     *
     * @return
     */
    private List<DiffUtilDemoEntity> getNewList() {
        List<DiffUtilDemoEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            /*
            Simulate deletion of data No. 1 and No. 3
            模拟删除1号和3号数据
             */
            if (i == 1 || i == 3) {
                continue;
            }

            /*
            Simulate modification title of data No. 0
            模拟修改0号数据的title
             */
            if (i == 0) {
                list.add(new DiffUtilDemoEntity(
                        i,
                        "😊Item " + i,
                        "This item " + i + " content",
                        "06-12")
                );
                continue;
            }

            /*
            Simulate modification content of data No. 4
            模拟修改4号数据的content发生变化
             */
            if (i == 4) {
                list.add(new DiffUtilDemoEntity(
                        i,
                        "Item " + i,
                        "Oh~~~~~~, Item " + i + " content have change",
                        "06-12")
                );
                continue;
            }

            list.add(new DiffUtilDemoEntity(
                    i,
                    "Item " + i,
                    "This item " + i + " content",
                    "06-12")
            );
        }
        return list;
    }

}
