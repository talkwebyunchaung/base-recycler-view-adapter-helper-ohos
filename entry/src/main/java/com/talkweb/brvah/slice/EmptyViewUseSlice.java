package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.QuickAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.util.DialogUtil;
import com.talkweb.brvah.util.LogUtils;
import com.talkweb.floatingbutton.FloatingActionButton;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;


/**
 * @author: jun.xiong
 * @date: 2021/5/11 12:00
 * @description:
 */
public class EmptyViewUseSlice extends BaseSlice {

    private ListContainer listContainer;

    private QuickAdapter adapter;
    private boolean mError = true;
    private boolean mNoData = true;
    private Component notDataView;
    private Component errorView;
    private Component emptyView;


    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_empty_view_use;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("EmptyView Use");
        listContainer = findView(ResourceTable.Id_list_empty);

        FloatingActionButton floatingActionButton = findView(ResourceTable.Id_fab_btn);
        floatingActionButton.setClickedListener(this);

        LayoutScatter  inflate = LayoutScatter.getInstance(getContext());
        notDataView = inflate.parse(ResourceTable.Layout_empty_view,  null, false);
//        notDataView = inflate.parse(ResourceTable.Layout_empty_view, (ComponentContainer) listContainer.getComponentParent(), false);
        notDataView.setClickedListener(v -> {
            onRefresh();
        });

        errorView = inflate.parse(ResourceTable.Layout_error_view,   null, false);
//        errorView = inflate.parse(ResourceTable.Layout_error_view, (ComponentContainer) listContainer.getComponentParent(), false);
        errorView.setClickedListener(v -> {
            onRefresh();
        });

        emptyView = inflate.parse(ResourceTable.Layout_loading_view,null,false);
//        emptyView = inflate.parse(ResourceTable.Layout_loading_view,(ComponentContainer)listContainer.getComponentParent(),false);
    }

    @Override
    public void onClick(Component component) {
        super.onClick(component);
        if (component.getId() == ResourceTable.Id_fab_btn) {
            LogUtils.syso("点击刷新");
            mError = true;
            mNoData = true;
            adapter.setNewData(null);
            onRefresh();
        }
    }

    @Override
    protected void initData() {
        adapter = new QuickAdapter(0);
        listContainer.setItemProvider(adapter);
        onRefresh();
        listContainer.setItemLongClickedListener(new ListContainer.ItemLongClickedListener() {
            @Override
            public boolean onItemLongClicked(ListContainer listContainer, Component component, int i, long l) {
                DialogUtil.toastShort(getContext(),"onLongClick:"+ i);
                return true;
            }
        });
    }

    private void onRefresh() {
        adapter.setEmptyView(emptyView);
        getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                if (mError) {
                    adapter.setEmptyView(errorView);
                    mError = false;
                } else {
                    if (mNoData) {
                        adapter.setEmptyView(notDataView);
                        mNoData = false;
                    } else {
                        adapter.setNewData(DataServer.getSampleData(10));
//                        listContainer.scrollTo(0);
                    }
                }
            }
        }, 1000);

    }
}
